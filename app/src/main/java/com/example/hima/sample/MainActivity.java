package com.example.hima.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Student student = new Student(1, "Джон Доу"); //создали экземпляр класса
        student.moveToNextYear(); //перевели студента на следующий курс

        //Джон Доу сейчас - просто студент. Создадим конкретно студента матфака:
        MathStudent mathStudent = new MathStudent(2, "Джейн Доу");

        //Джейн Доу - студент определённого факультета.
        //объект mathStudent имеет свойства и поведение как класса Student, так и класса mathStudent:
        mathStudent.moveToNextYear(); //метод из родительского класса
        mathStudent.goToDfm(); //метод из класса-наследника
        mathStudent.startSession(); //вызовется метод из класса MathStudent

        SportsEducationStudent sportStudent = new SportsEducationStudent(3, "Арни Чёрный");
        sportStudent.startSession();
    }

}
