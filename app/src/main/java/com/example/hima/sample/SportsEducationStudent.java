package com.example.hima.sample;


public class SportsEducationStudent extends Student {
    public SportsEducationStudent(int id, String name) {
        super(id, name);
    }

    @Override
    public void takePhysicalEducation() {
        //здесь происходит что-то посерьёзнее, чем в родительском классе
    }
}
