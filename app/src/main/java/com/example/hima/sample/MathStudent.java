package com.example.hima.sample;

public class MathStudent extends Student {

    public MathStudent(int id, String name) {
        super(id, name);
    }

    @Override
    public void startSession() {
        super.startSession();
        takeMatan();
        takeUmf();
    }

    private void takeMatan() {
        //здесь что-то происходит
    }

    private void takeUmf() {
        //здеь происходит что-то страшное
    }

    public void goToDfm() {

    }
}
