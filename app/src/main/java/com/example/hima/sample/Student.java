package com.example.hima.sample;

class Student {

    public int id; //номер зачётки
    public String name; //имя студента
    public int year; // курс

    interface NewInterface {
        void newVoid(); // только объявление метода
    }

    //Конструктор класса. При вызове создаёт объект с заданными параметрами.
    public Student(int id, String name) {
        this.id = id;
        this.name = name;
        this.year = 1;
    }

    //Метод класса. Описывает поведение, которое присуще экземплярам данного класса.
    public void moveToNextYear() {
        year++;
    }

    protected void startSession() {
        takePhysicalEducation();
        takeEnglish();
    }


    protected void takePhysicalEducation() {
        //здесь что-то происходит
    }

    protected void takeEnglish() {
        //здесь что-то происходит
    }
}
